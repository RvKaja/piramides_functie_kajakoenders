using System;

namespace Piramides_functie_KajaKoenders
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintLinksuitgelijnd(5);
            Console.WriteLine();
            PrintLinksuitgelijnd(5, '+');
            Console.WriteLine();
            PrintRechtssuitgelijnd(5, '+');
            Console.WriteLine();
            PrintGecentreerd(10, '+');
            Console.ReadLine();
        }

        static void PrintLinksuitgelijnd(int diepte, char symbool = '*')
        {   // for loop
            for (int i = 1; i <= diepte; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    //Symbool uitschrijven
                    Console.Write(symbool);
                }
                //Wit regel
                Console.WriteLine();
            }
        }

        static void PrintRechtssuitgelijnd(int diepte, char symbool = '*')
        {
            for (int i = 1; i <= diepte; i++)
            {
                for (int j = 1; j <= diepte; j++)
                {
                    if (j <= diepte - i)
                    {
                        Console.Write(' ');
                    }
                    else
                    {
                        Console.Write(symbool);
                    }
                }
                Console.WriteLine();
            }
        }

        static void PrintGecentreerd(int diepte, char symbool = '*')
        {
            for (int i = 1; i <= diepte; i++)
            {
                for (int j = 1; j < diepte + i; j++)
                {
                    if (j <= diepte - i)
                    {
                        Console.Write(' ');
                    }
                    else
                    {
                        Console.Write(symbool);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
